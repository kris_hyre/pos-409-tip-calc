﻿/*
 * Title: POS 409 Tip Calculator
 * Author: Kris Hyre
 * Date: 12/05/16
 * Location: University of Phoenix
 * Class: POS 409 .Net II
 * Instructor: Henry Williams
 * Description: This program will allow the user to enter a dollar amount, choose between 15% or 20% and calculate 
 *  the resulting tip and total amount.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS_409_Tip_Calculator
{
    public partial class Form1 : Form
    {
        // Variables needed
        private Decimal entered_value;
        private Decimal tip_value;
        private Decimal final_value;
        private String numeric_error = "Please enter a decimal value greater than 0.00 for the amount";

        // Constructor
        public Form1()
        {
            InitializeComponent();
        }

        // When the amount in the textbox entered by the user changes
        private void txt_ent_amt_TextChanged(object sender, EventArgs e)
        {
            // Check to see if the amount is a valid decimal amount, if so...
            if (Decimal.TryParse(txt_ent_amt.Text, out entered_value)) {
                entered_value = Math.Round(entered_value, 2); // ...round it off...

                int tip_percentage_setting = track_tip_percentage.Value; // ... get the tip percentage selected...
                Decimal tip_percentage;

                if (tip_percentage_setting == 0) { 
                    tip_percentage = 0.15M;
                } else {
                    tip_percentage = 0.20M;
                }

                tip_value = entered_value * tip_percentage; // ... calculate the tip amount, round, and display it...
                tip_value = Math.Round(tip_value, 2);
                txt_output.Text = tip_value.ToString("C");

                final_value = entered_value + tip_value; // ... and display the amount + tip.
                txt_combined_total.Text = final_value.ToString("C");

            } else {
                MessageBox.Show(numeric_error); // Otherwise, show an error and set the value to 0.00.
                txt_ent_amt.Text = "0.00";
            }
        }

        private void track_tip_percentage_Scroll(object sender, EventArgs e)
        {
            // If the tip percentage changes, treat as if new amount entered and recalculate.
            txt_ent_amt_TextChanged(sender, e);
        }
    }
}
