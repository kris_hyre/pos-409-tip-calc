﻿/*
 * Title: POS 409 Tip Calculator
 * Author: Kris Hyre
 * Date: 12/05/16
 * Location: University of Phoenix
 * Class: POS 409 .Net II
 * Instructor: Henry Williams
 * Description: This program will allow the user to enter a dollar amount, choose between 15% or 20% and calculate 
 *  the resulting tip.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS_409_Tip_Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
    }
}
