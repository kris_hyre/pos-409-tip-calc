﻿/*
 * Title: POS 409 Tip Calculator
 * Author: Kris Hyre
 * Date: 12/05/16
 * Location: University of Phoenix
 * Class: POS 409 .Net II
 * Instructor: Henry Williams
 * Description: This program will allow the user to enter a dollar amount, choose between 15% or 20% and calculate 
 *  the resulting tip.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS_409_Tip_Calculator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
